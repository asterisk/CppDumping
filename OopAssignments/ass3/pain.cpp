#include <iostream>
#include <istream>
#include "library/enumerator.hpp"
#include "library/seqinfileenumerator.hpp"
#include "library/stringstreamenumerator.hpp"
#include "library/summation.hpp"
#include "library/maxsearch.hpp"



using namespace std;

/* 1st question */

struct Fish
{
    string name;
    int size;
    int cnt;
    
    friend istream& operator>>(istream &is, Fish& f){
        is >> f.name >> f.size;
        return is;
    }

    Fish(){}
    Fish(string cn, int cavg, int fcnt): name(cn), size(cavg), cnt(fcnt) {}
};


struct Competitor
{
    string name;
    string id;
    double avg;
    int cnt; // fishes count

    bool isZero;

    
    friend istream& operator>>(istream& is, Competitor &c);

    Competitor(){}
    Competitor(string cname, string cid, double cavg, int ccnt): name (cname), id(cid), avg(cavg), cnt(ccnt) {}
};

/* Fish average ? */

class FishAvg : public Summation<Fish, Fish>
{
    Fish func(const Fish& f) const override { return Fish(f.name, f.size, 1);}
    Fish neutral() const override { return Fish("",0,0);}
    Fish add(const Fish& a, const Fish& b) const override{
         return Fish("avg", a.size + b.size, a.cnt + b.cnt);
    }
};

istream& operator>>(istream& is, Competitor &c)
{

    string line;
    getline(is, line, '\n');

    stringstream ss(line);
    ss >> c.name >> c.id;

    FishAvg f;
    StringStreamEnumerator<Fish> enor(ss);
    f.addEnumerator(&enor);
    f.run();
    c.cnt = f.result().cnt;
    c.avg = (f.result().cnt == 0) ?  0 : (f.result().size / f.result().cnt);
    c.isZero = f.result().size == 0;

    return is;
};


/* printing the big competitors */

class BigCompetitor: public Summation<Competitor, ostream>
{
    public:
        BigCompetitor(ostream *o) : Summation<Competitor, ostream>::Summation(o) { }
    protected:
        string func(const Competitor& e) const override {return e.name +" " +e.id+" "+ to_string(e.avg) + " \n";}
        bool cond(const Competitor& e) const override { return e.avg > 30; }
};

// Second Question

struct ZeroCom
{
    string name;
    int ZeroCnt;

    ZeroCom() {}
    ZeroCom(string n, int z): name(n), ZeroCnt(z) {}

};


class ZeroCounter : public Summation<Competitor, ZeroCom>
{
    private:
        string _name;
    public:
        ZeroCounter(const string &name) : _name(name) {}
    protected:
        ZeroCom func(const Competitor &e) const override { return ZeroCom(e.name, 1); }
        ZeroCom neutral() const override {return ZeroCom("", 0); }
        ZeroCom add (const ZeroCom& a, const ZeroCom& b) const override { return ZeroCom(a.name, a.ZeroCnt + b.ZeroCnt); }
        bool cond(const Competitor& e) { return e.isZero; }

        bool whileCond(const Competitor& e ) const override { return e.name == _name; }
        void first() override { }
};


class ZeroEnor : public Enumerator<ZeroCom>
{
    private:
        SeqInFileEnumerator<Competitor>* _f;
        ZeroCom _zerocom;
        bool _end;

    public:
        ZeroEnor(const string &fname) {_f = new SeqInFileEnumerator<Competitor>(fname);}
        ~ZeroEnor(){}
        void first() override { _f->first(); next();}
        void next() override;
        bool end() const override { return _end; }
        ZeroCom current() const override {return _zerocom; }
};

void ZeroEnor::next()
{
    _end = _f->end();
    if(_end) return;
    _zerocom.name = _f->current().name;
    ZeroCounter pr(_zerocom.name);
    pr.addEnumerator(_f);
    pr.run();
    _zerocom.ZeroCnt = pr.result().ZeroCnt;
}

class MaxZero: public MaxSearch<ZeroCom, int>
{
    protected:
        int func(const ZeroCom& e ) const override { return e.ZeroCnt; }
};


int main(){
    try {
        /* 1st */
        BigCompetitor pr(&cout);
        SeqInFileEnumerator<Competitor> enor("inp.txt");
        pr.addEnumerator(&enor);
        pr.run();

        /* 2nd */
        MaxZero pr2;
        ZeroEnor enor2("inp.txt");
        pr2.addEnumerator(&enor2);
        pr2.run();
        if(pr2.found())
            cout << " the maximum student with zero fishes is: " << pr2.optElem().name << endl;
        else
            cout << "There is no student with zero in the input file" << endl;
    } catch(SeqInFileEnumerator<Competitor>::Exceptions ex) {
        cout << "File not found!\n";
        return 1;
    }
    return 0;
}
