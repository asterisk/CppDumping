#pragma once

#include <functional>
#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <cstdlib>
#include "area.h"

using namespace std;

class Navigator{
    public:
        Navigator(string s);
        void print();
        void checkTypes();
        void nextRound();
        void maxWater();
        Area* getMaxWater();
        int getHum(){return _hum;}
        int getN(){return _n;};
        void destroy(){_areas.clear();};
        vector<Area*> getAreas() {return _areas;};
        int equals(Navigator n);

    private: 
        string _fname;
        vector<Area*> _areas;
        int _hum;
        int _n;
};