#include <iostream>
#include <fstream>
#include <string>
#include "area.h"

using namespace std;

void Plain::nextRound(char we) {
    switch (we) {
        case('S'): changeWater(-3); changeHum(5); break;
        case('C'): changeWater(-1); changeHum(5); break;
        case('T'): changeWater(+20); changeHum(5); break;
        case('R'): changeWater(+20); setHum(30); break;
    }
}

void GrassLand::nextRound(char we) {
    switch (we) {
        case('S'): changeWater(-6); changeHum(10); break;
        case('C'): changeWater(-2); changeHum(10); break;
        case('T'): changeWater(+15); changeHum(10); break;
        case('R'): changeWater(+15); setHum(30); break;
    }
}

void Lakes::nextRound(char we){
    switch (we) {
        case('S'): changeWater(-10); changeHum(15); break;
        case('C'): changeWater(-3); changeHum(15); break;
        case('T'): changeWater(+20); changeHum(15); break;
        case('R'): changeWater(+20); setHum(30); break;
    }
}
