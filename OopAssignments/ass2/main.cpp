// Author: Mohamed Abdelbary
// Date:  2022.5.1
// hydrological cycles of earth
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <string>
#include <vector>
#include "navigator.h"
#include "area.h"

//#include <bits/stdc++.h>

using namespace std;



#define NORMAL_MOD
#ifdef NORMAL_MOD
int main(){

    cout << "Enter file name: ";
    string fname; cin >> fname;
    Navigator nav(fname);

    for(int i=0; i<10; ++i){
        nav.print();
        nav.nextRound();
        nav.checkTypes();
        cout << "---------------- " << endl;
    }

    nav.print();
    cout << "---------------- " << endl;
    cout << "Max: "; nav.maxWater();

    return 0;
}
#else 
#define CATCH_CONFIG_MAIN
#include "catch.hpp"
TEST_CASE("next Rounds length and results","inp1*.txt"){
    Navigator nav1("inp11.txt");
    nav1.nextRound();
    CHECK(nav1.getN() == 1);

    Navigator out1("out111.txt");
    CHECK(nav1.equals(out1) == 1 );


    for(int i=0; i<9; ++i)
        nav1.nextRound();
    CHECK(nav1.getN() == 1);

    nav1.destroy();
    out1.destroy();

    Navigator nav2("inp12.txt");
    nav2.nextRound();
    CHECK(nav2.getN() == 2);

    Navigator out2("out121.txt");
    CHECK(nav2.equals(out2) == 1 );

    for(int i=0; i<9; ++i)
        nav2.nextRound();
    CHECK(nav2.getN() == 2);
    
    nav2.destroy();
    out2.destroy();

    Navigator nav3("inp13.txt");
    nav3.nextRound();
    CHECK(nav3.getN() == 4);
    

    Navigator out3("out131.txt");
    CHECK(nav3.equals(out3) == 1 );

    for(int i=0; i<9; ++i)
        nav3.nextRound();
    CHECK(nav3.getN() == 4);

    nav3.destroy();
    out3.destroy();

    Navigator nav4("inp14.txt");
    nav4.nextRound();
    CHECK(nav4.getN() == 8);

    Navigator out4("out141.txt");
    CHECK(nav4.equals(out4) == 1 );

    for(int i=0; i<9; ++i)
        nav4.nextRound();
    CHECK(nav4.getN() == 8);

    nav4.destroy();
    out4.destroy();
}

TEST_CASE("Check Types and results","inp2*.txt"){
    Navigator nav1("inp11.txt");
    nav1.nextRound();
    nav1.checkTypes();
    CHECK(nav1.getN() == 1);

    Navigator out1("out112.txt");
    CHECK(nav1.equals(out1) == 1 );


    for(int i=0; i<9; ++i){
        nav1.nextRound();
        nav1.checkTypes();
    }
    CHECK(nav1.getN() == 1);

    nav1.destroy();
    out1.destroy();

    Navigator nav2("inp12.txt");
    nav2.nextRound();
    nav2.checkTypes();
    CHECK(nav2.getN() == 2);

    Navigator out2("out122.txt");
    CHECK(nav2.equals(out2) == 1 );

    for(int i=0; i<9; ++i){
        nav2.nextRound();
        nav2.checkTypes();
    }
    CHECK(nav2.getN() == 2);
    
    nav2.destroy();
    out2.destroy();

    Navigator nav3("inp13.txt");
    nav3.nextRound();
    nav3.checkTypes();
    CHECK(nav3.getN() == 4);
    

    Navigator out3("out132.txt");
    CHECK(nav3.equals(out3) == 1 );

    for(int i=0; i<9; ++i){
        nav3.nextRound(); 
        nav3.checkTypes();
    }
    CHECK(nav3.getN() == 4);

    nav3.destroy();
    out3.destroy();

    Navigator nav4("inp14.txt");
    nav4.nextRound();
    nav4.checkTypes();
    CHECK(nav4.getN() == 8);

    Navigator out4("out142.txt");
    CHECK(nav4.equals(out4) == 1 );

    for(int i=0; i<9; ++i){
        nav4.nextRound();
        nav4.checkTypes();
    }
    CHECK(nav4.getN() == 8);
    nav4.destroy();
    out4.destroy();
}

TEST_CASE("max water after ten rounds", ""){
    Navigator nav1("inp3.txt");
    for(int i=0; i< 10; ++i){
        nav1.nextRound();
        nav1.checkTypes();
    }

    Area* l = nav1.getMaxWater();

    CHECK(l->getWater() == 104);
    CHECK(l->getPre() == "Mr");
    CHECK(l->getOwner() == "Bean");
    CHECK(l->getType() == 'L');

    nav1.destroy();
}



#endif