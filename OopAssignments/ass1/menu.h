#ifndef MENU_H
#define MENU_H
#include "mat.h"
#include <type_traits>
#include <vector>
using namespace std;

class Menu{
    public:
        Menu() : a(0), b(0) {};
        void run();
    private:
        void printMenu();
        void setFirstMat();
        void setSecondMat();
        void getElementFirstMat() const;
        void getElementSecondMat() const;
        void addMat();
        void multiMat();
        void printFirstMat();
        void printSecondMat();

    private:
        Mat a;
        Mat b;
};

#endif
