#ifndef  MAT_H
#define MAT_H
#include <iostream>
#include <type_traits>
#include <vector>

using namespace std;

class Mat{
    public:
        vector<int> arr;

    public:
        enum Exc{INCORRECTSIZE,DIFFERENTSIZES, WRONGINDEX};

        Mat(const int n);
        Mat(const vector<int> &a);

        void setVector(const vector<int> a){ arr = a; };

        int operator()(int i, int j) const; 
        friend Mat operator+ (Mat& a, Mat& b);
        friend Mat operator* (Mat& a, Mat& b);
        friend ostream& operator<< (ostream& s, const Mat& a);
        

};
#endif
