#include <iostream>
#include <vector>
#include "menu.h"

using namespace std;

void Menu::run(){
    int n=0;
    do
    {
        printMenu();
        cin >> n;
        switch (n) {
        case 1:
            setFirstMat();
            break;
        case 2:
            setSecondMat();
            break;
        case 3:
            getElementFirstMat();
            break;
        case 4:
            getElementSecondMat();
            break;
        case 5:
            addMat();
            break;
        case 6:
            multiMat();
            break;
        case 7:
            printFirstMat();
            break;
        case 8:
            printSecondMat();
            break;

        }
    } while(n!=0);
}

void Menu::printMenu(){
    cout << endl;
    cout << " 0. Quit" << endl;
    cout << " 1. Enter the first Matrix" << endl;
    cout << " 2. Enter the second Matrix" << endl;
    cout << " 3. Get an element from the first Matrix" << endl;
    cout << " 4. Get an element from the second Matrix" << endl;
    cout << " 5. Add both Matrices" << endl;
    cout << " 6. Mulityply both Matrices" << endl;
    cout << " 7. Print first Matrix" << endl;
    cout << " 8. Print Second Matrix" << endl;
    cout << "Command: ";
}
void Menu::setFirstMat(){
    cout << "Enter the matrix size: ";
    int n; cin >> n;
    vector<int> arr;
    cout << "Enter the non-zero elements: ";
    for(int i=0; i<n; ++i){
        int m; cin >> m;
        arr.push_back(m);
    }
    a.setVector(arr);
}
void Menu::setSecondMat(){
    cout << "Enter the matrix size: ";
    int n; cin >> n;
    vector<int> arr;
    cout << "Enter the non-zero elements: ";
    for(int i=0; i<n; ++i){
        int m; cin >> m;
        arr.push_back(m);
    }
    b.setVector(arr);
}

void Menu::getElementFirstMat() const{
    cout << "Enter the index of the row and the index of the column: ";
    int i,j;
    cin >> i >> j;
    try{
    cout << a(i-1,j-1) << endl;
    } catch(Mat::Exc x) {
        if(x == Mat::WRONGINDEX){
            cout << "Error! You wrote wrong index" << endl;
        }else{
            cout << "Error! Unkown Error" << endl;
        }
    }
}

void Menu::getElementSecondMat() const{
    cout << "Enter the index of the row and the index of the column: ";
    int i,j;
    cin >> i >> j;
    try{
    cout << b(i-1,j-1) << endl;
    } catch(Mat::Exc x) {
        if(x == Mat::WRONGINDEX){
            cout << "Error! You wrote wrong index" << endl;
        }else{
            cout << "Error! Unkown Error" << endl;
        }
    }
}

void Menu::addMat(){
    try {
        Mat c = a + b;
        cout << c << endl;
    } catch(Mat::Exc x){
        if( x == Mat::DIFFERENTSIZES){
            cout << "Error! The matrices have different size" << endl;
        } else {
            cout << "Error! Unkown Error";
        }
    }
}

void Menu::multiMat(){
    try {
        Mat c = a * b;
        cout << c << endl;
    } catch(Mat::Exc x){
        if( x == Mat::DIFFERENTSIZES){
            cout << "Error! The matrices have different size" << endl;
        } else {
            cout << "Error! Unkown Error";
        }
    }
}

void Menu::printFirstMat(){
    try{
    cout << a << endl;
    } catch(Mat::Exc x){
        cout << "ERROR, Unkown Error" << endl;
    }
}

void Menu::printSecondMat(){
    try{
    cout << b << endl;
    } catch(Mat::Exc x){
        cout << "ERROR, Unkown Error" << endl;
    }
}