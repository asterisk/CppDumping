#include "mat.h"
#include <iostream>
#include <ostream>
#include <type_traits>
#include <vector>

using namespace std;

Mat::Mat(int k){ (k>=0)? arr.resize(k) : throw INCORRECTSIZE; }

Mat::Mat(const vector<int> &a){ arr = a; }

int Mat::operator()(int i, int j) const {
    if(i >= arr.size() || j >= arr.size() || i < 0 || j < 0) 
        throw WRONGINDEX;
    return( (i==j) ? arr[i] : 0);
}

Mat operator+(Mat& a, Mat& b){
    if(a.arr.size() != b.arr.size()) 
        throw Mat::DIFFERENTSIZES;
    Mat c(a.arr.size());
    for(int i=0;i<a.arr.size();++i)
        c.arr[i] = a.arr[i] + b.arr[i];
    return c;
}

Mat operator*(Mat& a, Mat& b){
    if(a.arr.size() != b.arr.size()) 
        throw Mat::DIFFERENTSIZES;
    Mat c(a.arr.size());
    for(int i=0;i<a.arr.size();++i)
        c.arr[i] = a.arr[i] * b.arr[i];
    return c;
}

ostream& operator<<(ostream& os, const Mat& m){
    for( int i=0; i< m.arr.size(); ++i){
        for(int j=0; j< m.arr.size(); ++j){
            os << m(i, j) << " ";
        }
        os << "\n";
    }
    return os;
}
